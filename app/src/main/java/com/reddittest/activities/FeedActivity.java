package com.reddittest.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.reddittest.R;
import com.reddittest.adapters.FeedAdapter;
import com.reddittest.inc.Internet;
import com.reddittest.utils.EndlessRecyclerOnScrollListener;
import com.reddittest.ws.Rest;
import com.reddittest.ws.interfaces.WsFeedInterface;
import com.reddittest.ws.models.WsChild;
import com.reddittest.ws.models.WsFeed;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gpbcousin on 24/10/16.
 */

public class FeedActivity extends DefaultActivity {
    @Bind(R.id.m_recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.sl_swipe)
    SwipeRefreshLayout slSwipe;

    private EndlessRecyclerOnScrollListener scroll;

    StickyRecyclerHeadersDecoration headersDecor;

    FeedAdapter adapter;


    private List<WsChild> mList = new ArrayList<>();
    private String lastId = null;

    @Override
    protected void init() {
        mLayout = R.layout.activity_feed;
        mExibirHomeButton = false;
        mTitle = R.string.app_name;

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initDadosView() {


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mCtx);


        adapter = new FeedAdapter(mCtx);
        adapter.setList(mList);

        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setItemViewCacheSize(0);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        headersDecor = new StickyRecyclerHeadersDecoration(adapter);
        mRecyclerView.setAdapter(adapter);

        scroll = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (Internet.temConexao(mCtx)) {
                    buscaDados();
                } else {
                    Internet.msgSemConexao(mCtx, mRecyclerView);
                }
            }
        };

        slSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lastId = null;
                mList.clear();
                adapter.notifyDataSetChanged();
                scroll.reset();
                buscaDados();
            }
        });
        mRecyclerView.addItemDecoration(headersDecor);
        mRecyclerView.addOnScrollListener(scroll);


        buscaDados();
    }

    private void buscaDados() {
        progressExibe();
        WsFeedInterface service = Rest.getRetrofit(mCtx).create(WsFeedInterface.class);
        Call<WsFeed> call = service.getItens(lastId);
        call.enqueue(new Callback<WsFeed>() {
            @Override
            public void onResponse(Call<WsFeed> call, Response<WsFeed> response) {
                progressEsconde();
                slSwipe.setRefreshing(false);
                if (response.isSuccessful()) {
                    mList.addAll(response.body().getData().getChildren());
                    adapter.setList(mList);
                    adapter.notifyDataSetChanged();
                    lastId = response.body().getData().getAfter();
                }
            }

            @Override
            public void onFailure(Call<WsFeed> call, Throwable t) {
                progressEsconde();
                slSwipe.setRefreshing(false);
                Internet.msgSemConexao(mCtx, mRecyclerView);
            }
        });
    }


}
