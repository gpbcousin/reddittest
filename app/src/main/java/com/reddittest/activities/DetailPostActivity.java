package com.reddittest.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.reddittest.R;
import com.reddittest.adapters.PostDatailAdapter;
import com.reddittest.inc.ArgsDefault;
import com.reddittest.inc.Internet;
import com.reddittest.ws.Rest;
import com.reddittest.ws.interfaces.WsPostInterface;
import com.reddittest.ws.models.WsChild;
import com.reddittest.ws.models.WsFeed;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gpbcousin on 30/10/16.
 */

public class DetailPostActivity extends DefaultActivity {
    @Bind(R.id.m_recyclerView)
    RecyclerView mRecyclerView;


    WsChild data;
    PostDatailAdapter adapter;

    private List<WsChild> mList = new ArrayList<>();


    @Override
    protected void init() {
        mLayout = R.layout.activity_detalh_post;
    }

    @Override
    protected void initView() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void getArgumentsIntent() {
        super.getArgumentsIntent();
        Intent it = getIntent();
        if (it != null) {
            data = it.getParcelableExtra(ArgsDefault.POST);
        }
        if (data != null) {
            mList.add(data);
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }

        }


    }

    public void buscaDados() {
        WsPostInterface service = Rest.getRetrofit(mCtx).create(WsPostInterface.class);
        Call<List<WsFeed>> call = service.getPostDetail(data.getData().getId());
        call.enqueue(new Callback<List<WsFeed>>() {
            @Override
            public void onResponse(Call<List<WsFeed>> call, Response<List<WsFeed>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().size() > 1) {
                        data = response.body().get(0).getData().getChildren().get(0);
                        mList.clear();
                        mList.add(data);
                        mList.addAll(response.body().get(1).getData().getChildren());
                        adapter.setList(mList);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WsFeed>> call, Throwable t) {
                Internet.msgSemConexao(mCtx, mRecyclerView);
            }
        });
    }

    @Override
    protected void initDadosView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mCtx);


        adapter = new PostDatailAdapter(mCtx);
        adapter.setList(mList);

        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setItemViewCacheSize(0);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                buscaDados();
            }
        }, 600);
    }

}
