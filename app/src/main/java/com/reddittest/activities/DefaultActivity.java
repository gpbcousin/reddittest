package com.reddittest.activities;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;

import com.reddittest.R;
import com.reddittest.inc.Erro;

import butterknife.ButterKnife;

/**
 * Created by gpbcousin on 24/10/16.
 */


abstract public class DefaultActivity extends AppCompatActivity {

    protected String mTituloAnalytics;

    protected int mLayout;
    protected int mTitle;
    protected boolean mExibirHomeButton = true;
    protected AppCompatActivity mCtx;
    protected ProgressBar mProgress;
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = this;
        init();

        setContentView(mLayout);
        ButterKnife.bind(this);
        getArgumentsIntent();
        recuperarSavedInstanceState(savedInstanceState);

        mProgress = (ProgressBar) findViewById(R.id.progress);

        if (mProgress != null) {
            mProgress.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        }

        initToolbar();
        initView();
        initDadosView();
    }

    @Override
    protected void onStart() {
        super.onStart();


    }


    protected abstract void init();

    protected abstract void initView();

    protected abstract void initDadosView();

    protected void initToolbar() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);

            if (mTitle > 0) {
                getSupportActionBar().setTitle(mTitle);
            } else {
                getSupportActionBar().setTitle("");
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(mExibirHomeButton);
            getSupportActionBar().setHomeButtonEnabled(mExibirHomeButton);
        }
    }

    protected void sair() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                sair();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void getArgumentsIntent() {
    }

    protected void logout() {

    }

    protected void recuperarSavedInstanceState(Bundle savedInstanceState) {
    }

    protected void proximaTelaDeay(final Class<?> cls) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(mCtx, cls));
            }
        }, 400);

    }

    protected void progressExibe() {

        if (mProgress != null) {

            mProgress.setScaleX(0f);
            mProgress.setScaleY(0f);
            mProgress.setVisibility(View.VISIBLE);

            mProgress.animate()
                    .scaleX(1)
                    .scaleY(1)
                    .setDuration(350)
                    .setInterpolator(new OvershootInterpolator())
                    .setStartDelay(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mProgress.setVisibility(View.VISIBLE);
                        }
                    })
                    .start();
        }
    }

    protected void progressEsconde() {

        if (mProgress != null) {

            mProgress.animate()
                    .scaleX(0f)
                    .scaleY(0f)
                    .setDuration(450)
                    .setInterpolator(new OvershootInterpolator())
                    .setStartDelay(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mProgress.setVisibility(View.INVISIBLE);
                        }
                    })
                    .start();

        }
    }

    protected void snackErro(View view, String msg) {
        Erro.snack(mCtx, msg, view, R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickSnackAction();
            }
        });
    }

    protected void clickSnackAction() {
    }

    protected void trackerEvent(String category, String action, String label) {

    }

    protected void trackerScreen(String screeName) {

    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
        super.onBackPressed();
    }
}