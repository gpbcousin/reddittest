package com.reddittest.inc;

/**
 * Created by gpbcousin on 24/10/16.
 */

public class ArgsDefault {
    public static final String POST = "Post";
    public static int TYPE_VIDEO = 0;
    public static int TYPE_IMAGE = 1;
    public static int TYPE_DESCRIPTION = 2;
    public static int TYPE_COMMENT = 3;
}
