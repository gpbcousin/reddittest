package com.reddittest.inc;

import android.app.Activity;
import android.content.Context;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.reddittest.R;

import java.util.Date;


public class Config {

    private Config() {
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static boolean isLogErro(Context ctx) {
        return ctx.getResources().getBoolean(R.bool.ConfigLogErro);
    }

    public static boolean isProduction(Context ctx) {
        return ctx.getResources().getBoolean(R.bool.ConfigProduction);
    }

    public static boolean isTeste(Context ctx) {
        return ctx.getResources().getBoolean(R.bool.ConfigTeste);
    }

    public static boolean isDev(Context ctx) {
        return ctx.getResources().getBoolean(R.bool.ConfigDev);
    }

    public static int getVersioCodeApp(Context ctx) {

        int appVer = 0;

        try {
            appVer = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            Erro.setErro(ctx, "erro", e);
        }

        return appVer;
    }

    public static String getPackgeApp(Context ctx) {
        String pkg;

        try {
            pkg = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).packageName;
        } catch (Exception e) {
            Erro.setErro(ctx, "erro", e);

            pkg = "com.reddittest";
        }

        return pkg;
    }



    public static boolean checkPlayServices(Activity ctx) {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(ctx);

        if (resultCode != ConnectionResult.SUCCESS) {

            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(ctx, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }
}
