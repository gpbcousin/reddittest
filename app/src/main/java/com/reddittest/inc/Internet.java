package com.reddittest.inc;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.reddittest.R;

/**
 * Created by gpbcousin on 24/10/16.
 */


public class Internet {

    private Internet() {
    }

    public static boolean temConexao(Context ctx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm != null && cm.getActiveNetworkInfo() != null) {
                return (cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
            } else {
                return false;
            }
        } catch (Exception e) {
            Erro.setErro(ctx, "erro", e);
        }
        return false;
    }

    public static void msgSemConexao(Context ctx, View view, View.OnClickListener listener) {
        Snackbar snack = Snackbar.make(view, R.string.sem_conexao, Snackbar.LENGTH_INDEFINITE);
        snack.getView().setBackgroundColor(ctx.getResources().getColor(R.color.vermelho));
        snack.setActionTextColor(ctx.getResources().getColor(R.color.branco));
        snack.setAction(R.string.ok, listener);
        snack.show();
    }

    public static void msgSemConexao(Context ctx, View view) {
        msgSemConexao(ctx, view, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}