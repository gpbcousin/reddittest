package com.reddittest.inc;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;
import com.reddittest.R;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by gpbcousin on 24/10/16.
 */


public class Erro {

    private Erro() {
    }

    /**
     * ULTIMO ERRO E000070 PROXIMO E000071
     */
    public static void setErro(Context ctx, String erro, String msgErro) {
        try {

            if (Config.isLogErro(ctx)) {
                Log.e("ERRO: " + erro, msgErro);
                Toast.makeText(ctx, "ERRO: " + erro, Toast.LENGTH_LONG).show();
            }

            String dados = erro + " - " + msgErro;
            Crashlytics.log(dados);

        } catch (Exception e) {

        }
    }

    public static void setErro(Context ctx, String erro, Throwable e) {
        try {
//
           Crashlytics.logException(e);

            if (Config.isLogErro(ctx)) {
                Log.e("ERRO: ", erro);
                Toast.makeText(ctx, "ERRO: " + erro, Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            String dados = erro + ": " + Build.MANUFACTURER + " " + Build.MODEL + " " + Build.VERSION.RELEASE;
//            Evento.trackerException(ctx, dados, e);

        } catch (Exception f) {

        }
    }

    public static void setErroWs(Context ctx, String erro, Throwable e) {
        try {

            if (e != null && !(e instanceof ConnectException
                    || e instanceof UnknownHostException
                    || e instanceof SocketException
                    || e instanceof SocketTimeoutException
            )) {

                setErro(ctx, erro, e);
            }

        } catch (Exception f) {
        }
    }

    public static void snack(Context ctx, @StringRes int msg, @NonNull View view) {
        snack(ctx, ctx.getString(msg), view);
    }

    public static void snack(Context ctx, String msg, @NonNull View view) {
        snack(ctx, msg, view, R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    public static void snack(Context ctx, String msg, @NonNull View view, @StringRes int buttonText, View.OnClickListener onClick) {
        Snackbar snack = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE);
        snack.getView().setBackgroundColor(ctx.getResources().getColor(R.color.vermelho));
        snack.setActionTextColor(ctx.getResources().getColor(R.color.branco));
        snack.setAction(buttonText, onClick);
        snack.show();
    }
}