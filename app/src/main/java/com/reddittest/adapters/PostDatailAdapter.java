package com.reddittest.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.reddittest.R;
import com.reddittest.inc.ArgsDefault;
import com.reddittest.utils.DateUtils;
import com.reddittest.views.RobotoTextView;
import com.reddittest.ws.models.WsChild;
import com.reddittest.ws.models.WsFeed;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by gpbcousin on 30/10/16.
 */

public class PostDatailAdapter extends RecyclerView.Adapter<PostDatailAdapter.Holder> {
    public List<WsChild> mList;
    Context mCtx;

    public PostDatailAdapter(Context ctx) {
        mCtx = ctx;
    }

    public void setList(List<WsChild> mList) {
        this.mList = mList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mCtx);
        if (viewType == ArgsDefault.TYPE_VIDEO) {
            View root = inflater.inflate(R.layout.item_video_view_post, viewGroup, false);
            return new ItemVideo(root);
        } else if (viewType == ArgsDefault.TYPE_IMAGE) {
            View root = inflater.inflate(R.layout.item_image_view_post, viewGroup, false);
            return new ItemImagem(root);
        } else if (viewType == ArgsDefault.TYPE_DESCRIPTION) {
            View root = inflater.inflate(R.layout.item_text_view_post, viewGroup, false);
            return new ItemDescriptiom(root);
        } else if (viewType == ArgsDefault.TYPE_COMMENT) {
            View root = inflater.inflate(R.layout.item_comment_view_feed, viewGroup, false);
            return new ItemComment(root);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindItem(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList.get(position).getData().getPostHint() != null) {
            if (mList.get(position).getData().getPostHint().equals("rich:video")) {
                return ArgsDefault.TYPE_VIDEO;
            } else if (mList.get(position).getData().getPostHint().equals("link")) {
                return ArgsDefault.TYPE_IMAGE;
            } else {
                return ArgsDefault.TYPE_DESCRIPTION;
            }
        } else {
            if (!mList.get(position).getKind().equals("t1")) {
                if (mList.get(position).getData().getPostHint() != null || mList.get(position).getData().getUrl() != null) {
                    return ArgsDefault.TYPE_DESCRIPTION;
                } else {
                    return ArgsDefault.TYPE_IMAGE;
                }
            } else {
                return ArgsDefault.TYPE_COMMENT;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static abstract class Holder extends RecyclerView.ViewHolder {


        public Holder(View view) {
            super(view);
        }

        abstract public void bindItem(int position);
    }


    class ItemVideo extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.iv_thumbnail)
        ImageView ivThumbnail;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;

        WsChild dto;

        public ItemVideo(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            dto = mList.get(position);
            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));

            if (dto.getData().getPreview().getImages() != null) {
                Glide.with(mCtx).load(dto.getData().getPreview().getImages().get(0).getSource().getUrl()).into(ivThumbnail);
            } else {
                Glide.with(mCtx).load(dto.getData().getPreview()).into(ivThumbnail);
            }


            ivThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCtx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(dto.getData().getUrl())));

                }
            });
        }


    }


    class ItemImagem extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.iv_thumbnail)
        ImageView ivThumbnail;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;
        @Bind(R.id.tv_description)
        RobotoTextView tvDescription;
        @Bind(R.id.tv_link)
        RobotoTextView tvLink;

        WsChild dto;

        public ItemImagem(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            dto = mList.get(position);
            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));

            if (dto.getData().getUrl() != null) {
                tvLink.setVisibility(View.VISIBLE);
                tvLink.setText(dto.getData().getUrl());
                tvLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = dto.getData().getUrl();
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                        builder.setToolbarColor(mCtx.getResources().getColor(R.color.colorPrimary));
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.launchUrl(mCtx, Uri.parse(url));

                    }
                });
            } else {
                tvLink.setVisibility(View.GONE);
            }

            if (dto.getData().getSelftext() != null) {
                tvDescription.setText(dto.getData().getSelftext());
                tvDescription.setVisibility(View.VISIBLE);
            } else {
                tvDescription.setVisibility(View.GONE);

            }


            if (dto.getData().getPreview().getImages() != null) {
                Glide.with(mCtx).load(dto.getData().getPreview().getImages().get(0).getSource().getUrl()).into(ivThumbnail);
            } else {
                Glide.with(mCtx).load(dto.getData().getPreview()).into(ivThumbnail);
            }

        }


    }

    class ItemDescriptiom extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.tv_description)
        RobotoTextView tvDescription;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;
        @Bind(R.id.tv_link)
        RobotoTextView tvLink;

        WsChild dto;

        public ItemDescriptiom(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));
            if (dto.getData().getSelftext() != null) {
                tvDescription.setText(dto.getData().getSelftext());
                tvDescription.setVisibility(View.VISIBLE);
            } else {
                tvDescription.setVisibility(View.GONE);

            }
            if (dto.getData().getUrl() != null) {
                tvLink.setVisibility(View.VISIBLE);
                tvLink.setText(dto.getData().getUrl());
                tvLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = dto.getData().getUrl();
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                        builder.setToolbarColor(mCtx.getResources().getColor(R.color.colorPrimary));
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.launchUrl(mCtx, Uri.parse(url));

                    }
                });
            } else {
                tvLink.setVisibility(View.GONE);
            }


        }
    }

    class ItemComment extends Holder {
        @Bind(R.id.tv_description)
        RobotoTextView tvDescription;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;

        WsChild dto;
        WsFeed comments;

        PostDatailAdapter adapter;

        public ItemComment(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));
            tvDescription.setText(dto.getData().getBody());
        }

    }


}
