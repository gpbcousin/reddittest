package com.reddittest.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.reddittest.R;
import com.reddittest.activities.DetailPostActivity;
import com.reddittest.inc.ArgsDefault;
import com.reddittest.utils.DateUtils;
import com.reddittest.views.RobotoTextView;
import com.reddittest.ws.models.WsChild;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by gpbcousin on 24/10/16.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.Holder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {
    public List<WsChild> mList;
    Context mCtx;

    public FeedAdapter(Context ctx) {
        mCtx = ctx;
    }

    public void setList(List<WsChild> mList) {
        this.mList = mList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mCtx);
        if (viewType == ArgsDefault.TYPE_VIDEO) {
            View root = inflater.inflate(R.layout.item_video_view_feed, viewGroup, false);
            return new ItemVideo(root);
        } else if (viewType == ArgsDefault.TYPE_IMAGE) {
            View root = inflater.inflate(R.layout.item_image_view_feed, viewGroup, false);
            return new ItemImagem(root);
        } else if (viewType == ArgsDefault.TYPE_DESCRIPTION) {
            View root = inflater.inflate(R.layout.item_text_view_feed, viewGroup, false);
            return new ItemDescriptiom(root);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bindItem(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (mList.get(position).getData().getPostHint() != null) {
            if (mList.get(position).getData().getPostHint().equals("rich:video")) {
                return ArgsDefault.TYPE_VIDEO;
            } else if (mList.get(position).getData().getPostHint().equals("link")) {
                return ArgsDefault.TYPE_IMAGE;
            } else {
                return ArgsDefault.TYPE_DESCRIPTION;
            }
        } else {
            if (mList.get(position).getData().getPostHint() != null || mList.get(position).getData().getUrl() != null) {
                return ArgsDefault.TYPE_DESCRIPTION;
            } else {
                return ArgsDefault.TYPE_IMAGE;
            }
        }
    }

    @Override
    public long getHeaderId(int position) {
        long id = mList.get(position).getData().getAuthor().hashCode();
        if (id < 0) {
            id = id * -1;
        }
        return id;

    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_feed, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        RobotoTextView title = (RobotoTextView) holder.itemView.findViewById(R.id.tv_name_autor);
        title.setText(mList.get(position).getData().getAuthor());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static abstract class Holder extends RecyclerView.ViewHolder {


        public Holder(View view) {
            super(view);
        }

        abstract public void bindItem(int position);
    }


    class ItemVideo extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.iv_thumbnail)
        ImageView ivThumbnail;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;

        WsChild dto;


        View view;

        public ItemVideo(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            dto = mList.get(position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent it = new Intent(mCtx, DetailPostActivity.class);
                    it.putExtra(ArgsDefault.POST, dto);
                    mCtx.startActivity(it);
                }
            });
            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));

            ivThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCtx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(dto.getData().getUrl())));

                }
            });

            if (dto.getData().getPreview().getImages() != null) {
                Glide.with(mCtx).load(dto.getData().getPreview().getImages().get(0).getSource().getUrl()).into(ivThumbnail);
            } else {
                Glide.with(mCtx).load(dto.getData().getPreview()).into(ivThumbnail);
            }

        }


    }


    class ItemImagem extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.iv_thumbnail)
        ImageView ivThumbnail;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;

        WsChild dto;

        View view;

        public ItemImagem(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent it = new Intent(mCtx, DetailPostActivity.class);
                    it.putExtra(ArgsDefault.POST, dto);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation((AppCompatActivity) mCtx, ivThumbnail, "image_tbl");

                        mCtx.startActivity(it, options.toBundle());
                    } else {
                        mCtx.startActivity(it);
                    }
                }
            });
            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));

            if (dto.getData().getPreview().getImages() != null) {
                Glide.with(mCtx).load(dto.getData().getPreview().getImages().get(0).getSource().getUrl()).into(ivThumbnail);
            } else {
                Glide.with(mCtx).load(dto.getData().getPreview()).into(ivThumbnail);
            }

        }


    }

    class ItemDescriptiom extends Holder {
        @Bind(R.id.tv_title)
        RobotoTextView tvTitle;
        @Bind(R.id.tv_description)
        RobotoTextView tvDescription;
        @Bind(R.id.tv_name_autor)
        RobotoTextView tvNameAutor;
        @Bind(R.id.tv_created)
        RobotoTextView tvCreated;
        @Bind(R.id.tv_num_comment)
        RobotoTextView tvNumComment;
        @Bind(R.id.tv_num_like)
        RobotoTextView tvNumLike;
        @Bind(R.id.tv_link)
        RobotoTextView tvLink;

        WsChild dto;
        View view;

        public ItemDescriptiom(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

        }

        @Override
        public void bindItem(int position) {
            dto = mList.get(position);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent it = new Intent(mCtx, DetailPostActivity.class);
                    it.putExtra(ArgsDefault.POST, dto);
                    mCtx.startActivity(it);
                }
            });

            tvTitle.setText(dto.getData().getTitle());
            tvNameAutor.setText(dto.getData().getAuthor());
            tvNumComment.setText(String.format("%d", dto.getData().getNumComments()));
            tvNumLike.setText(String.format("%d", dto.getData().getScore()));
            tvCreated.setText(DateUtils.formatElapsedTime(dto.getData().getCreatedUtc() * 1000));


            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(350);
            tvDescription.setFilters(fArray);

            if (dto.getData().getSelftext() != null) {
                tvDescription.setVisibility(View.VISIBLE);
                tvDescription.setText(dto.getData().getSelftext());
            } else {
                tvDescription.setVisibility(View.GONE);
            }
            if (dto.getData().getUrl() != null) {
                tvLink.setVisibility(View.VISIBLE);
                tvLink.setText(dto.getData().getUrl());
                tvLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = dto.getData().getUrl();
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                        builder.setToolbarColor(mCtx.getResources().getColor(R.color.colorPrimary));
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.launchUrl(mCtx, Uri.parse(url));

                    }
                });
            } else {
                tvLink.setVisibility(View.GONE);
            }


        }
    }


}
