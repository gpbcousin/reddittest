package com.reddittest.ws;

import android.content.Context;

import com.reddittest.R;
import com.reddittest.inc.Config;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gpbcousin on 24/10/16.
 */

public class Rest {
    Context mCtx;

    public Rest(Context mCtx) {
        this.mCtx = mCtx;
    }


    private static Retrofit mRestAdapter;

    public static Retrofit getRetrofit(Context mCtx) {

        if (mRestAdapter == null) {
            String apiURM = mCtx.getString(R.string.ApiURL);

            OkHttpClient client;

            if (Config.isDev(mCtx) || Config.isTeste(mCtx)) {

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                client = new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        .build();

            } else {

                client = new OkHttpClient.Builder()
                        .build();

            }

            mRestAdapter = new Retrofit.Builder()
                    .baseUrl(apiURM)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return mRestAdapter;
    }

}
