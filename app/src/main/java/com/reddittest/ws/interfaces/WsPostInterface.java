package com.reddittest.ws.interfaces;

import com.reddittest.ws.models.WsFeed;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by gpbcousin on 29/10/16.
 */

public interface WsPostInterface {

    @GET("comments/article.json")
    Call<List<WsFeed>> getPostDetail(@Query("article") String id);
}
