package com.reddittest.ws.interfaces;

import com.reddittest.ws.models.WsFeed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by gpbcousin on 25/10/16.
 */

public interface WsFeedInterface {
    @GET("new/.json")
    Call<WsFeed> getItens(@Query("after") String after);
}
