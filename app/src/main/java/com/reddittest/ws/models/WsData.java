
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WsData implements Parcelable {

    @SerializedName("modhash")
    private String modhash;
    @SerializedName("children")
    private List<WsChild> children = new ArrayList<WsChild>();
    @SerializedName("after")
    private String after;
    @SerializedName("before")
    private String before;

    public WsData() {
    }

    public WsData(String modhash, List<WsChild> children, String after, String before) {
        this.modhash = modhash;
        this.children = children;
        this.after = after;
        this.before = before;
    }


    protected WsData(Parcel in) {
        modhash = in.readString();
        children = in.createTypedArrayList(WsChild.CREATOR);
        after = in.readString();
        before = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(modhash);
        dest.writeTypedList(children);
        dest.writeString(after);
        dest.writeString(before);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsData> CREATOR = new Creator<WsData>() {
        @Override
        public WsData createFromParcel(Parcel in) {
            return new WsData(in);
        }

        @Override
        public WsData[] newArray(int size) {
            return new WsData[size];
        }
    };

    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    public List<WsChild> getChildren() {
        return children;
    }

    public void setChildren(List<WsChild> children) {
        this.children = children;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

}
