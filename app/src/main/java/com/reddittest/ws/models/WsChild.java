
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WsChild implements Parcelable{

    @SerializedName("kind")
    private String kind;
    @SerializedName("data")
    private WsChildData data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public WsChildData getData() {
        return data;
    }

    public void setData(WsChildData data) {
        this.data = data;
    }

    protected WsChild(Parcel in) {
        kind = in.readString();
        data = in.readParcelable(WsChildData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kind);
        dest.writeParcelable(data, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsChild> CREATOR = new Creator<WsChild>() {
        @Override
        public WsChild createFromParcel(Parcel in) {
            return new WsChild(in);
        }

        @Override
        public WsChild[] newArray(int size) {
            return new WsChild[size];
        }
    };
}
