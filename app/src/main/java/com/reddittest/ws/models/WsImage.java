
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WsImage implements Parcelable {

    @SerializedName("source")
    private WsSource source;
    @SerializedName("id")
    private String id;

    public WsImage() {
    }


    public WsSource getSource() {
        return source;
    }

    public void setSource(WsSource source) {
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected WsImage(Parcel in) {
        source = in.readParcelable(WsSource.class.getClassLoader());
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(source, flags);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsImage> CREATOR = new Creator<WsImage>() {
        @Override
        public WsImage createFromParcel(Parcel in) {
            return new WsImage(in);
        }

        @Override
        public WsImage[] newArray(int size) {
            return new WsImage[size];
        }
    };


}
