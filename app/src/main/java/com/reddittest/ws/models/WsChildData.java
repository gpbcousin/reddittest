
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WsChildData implements Parcelable {

    @SerializedName("contest_mode")
    private Boolean contestMode;
    @SerializedName("domain")
    private String domain;
    @SerializedName("subreddit")
    private String subreddit;
    @SerializedName("selftext_html")
    private String selftextHtml;
    @SerializedName("selftext")
    private String selftext;
    @SerializedName("saved")
    private Boolean saved;
    @SerializedName("id")
    private String id;
    @SerializedName("gilded")
    private int gilded;
    @SerializedName("clicked")
    private Boolean clicked;
    @SerializedName("author")
    private String author;
    @SerializedName("name")
    private String name;
    @SerializedName("score")
    private int score;
    @SerializedName("over_18")
    private Boolean over18;
    @SerializedName("hidden")
    private Boolean hidden;
    @SerializedName("preview")
    private WsPreview preview;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("subreddit_id")
    private String subredditId;
    @SerializedName("author_flair_css_class")
    private String authorFlairCssClass;
    @SerializedName("downs")
    private int downs;
    @SerializedName("archived")
    private Boolean archived;
    @SerializedName("post_hint")
    private String postHint;
    @SerializedName("is_self")
    private Boolean isSelf;
    @SerializedName("hide_score")
    private Boolean hideScore;
    @SerializedName("permalink")
    private String permalink;
    @SerializedName("locked")
    private Boolean locked;
    @SerializedName("stickied")
    private Boolean stickied;
    @SerializedName("created")
    private long created;
    @SerializedName("url")
    private String url;
    @SerializedName("author_flair_text")
    private String authorFlairText;
    @SerializedName("quarantine")
    private Boolean quarantine;
    @SerializedName("title")
    private String title;
    @SerializedName("created_utc")
    private long createdUtc;
    @SerializedName("num_comments")
    private int numComments;
    @SerializedName("visited")
    private Boolean visited;
    @SerializedName("ups")
    private int ups;
    @SerializedName("replies")
    private Object replies;
    @SerializedName("body")
    private String body;

    protected WsChildData(Parcel in) {
        domain = in.readString();
        subreddit = in.readString();
        selftextHtml = in.readString();
        selftext = in.readString();
        id = in.readString();
        gilded = in.readInt();
        author = in.readString();
        name = in.readString();
        score = in.readInt();
        preview = in.readParcelable(WsPreview.class.getClassLoader());
        thumbnail = in.readString();
        subredditId = in.readString();
        authorFlairCssClass = in.readString();
        downs = in.readInt();
        postHint = in.readString();
        permalink = in.readString();
        created = in.readLong();
        url = in.readString();
        authorFlairText = in.readString();
        title = in.readString();
        createdUtc = in.readLong();
        numComments = in.readInt();
        ups = in.readInt();
        body = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(domain);
        dest.writeString(subreddit);
        dest.writeString(selftextHtml);
        dest.writeString(selftext);
        dest.writeString(id);
        dest.writeInt(gilded);
        dest.writeString(author);
        dest.writeString(name);
        dest.writeInt(score);
        dest.writeParcelable(preview, flags);
        dest.writeString(thumbnail);
        dest.writeString(subredditId);
        dest.writeString(authorFlairCssClass);
        dest.writeInt(downs);
        dest.writeString(postHint);
        dest.writeString(permalink);
        dest.writeLong(created);
        dest.writeString(url);
        dest.writeString(authorFlairText);
        dest.writeString(title);
        dest.writeLong(createdUtc);
        dest.writeInt(numComments);
        dest.writeInt(ups);
        dest.writeString(body);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsChildData> CREATOR = new Creator<WsChildData>() {
        @Override
        public WsChildData createFromParcel(Parcel in) {
            return new WsChildData(in);
        }

        @Override
        public WsChildData[] newArray(int size) {
            return new WsChildData[size];
        }
    };

    public Boolean getContestMode() {
        return contestMode;
    }

    public void setContestMode(Boolean contestMode) {
        this.contestMode = contestMode;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public String getSelftextHtml() {
        return selftextHtml;
    }

    public void setSelftextHtml(String selftextHtml) {
        this.selftextHtml = selftextHtml;
    }

    public String getSelftext() {
        return selftext;
    }

    public void setSelftext(String selftext) {
        this.selftext = selftext;
    }

    public Boolean getSaved() {
        return saved;
    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGilded() {
        return gilded;
    }

    public void setGilded(int gilded) {
        this.gilded = gilded;
    }

    public Boolean getClicked() {
        return clicked;
    }

    public void setClicked(Boolean clicked) {
        this.clicked = clicked;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Boolean getOver18() {
        return over18;
    }

    public void setOver18(Boolean over18) {
        this.over18 = over18;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public WsPreview getPreview() {
        return preview;
    }

    public void setPreview(WsPreview preview) {
        this.preview = preview;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSubredditId() {
        return subredditId;
    }

    public void setSubredditId(String subredditId) {
        this.subredditId = subredditId;
    }

    public String getAuthorFlairCssClass() {
        return authorFlairCssClass;
    }

    public void setAuthorFlairCssClass(String authorFlairCssClass) {
        this.authorFlairCssClass = authorFlairCssClass;
    }

    public int getDowns() {
        return downs;
    }

    public void setDowns(int downs) {
        this.downs = downs;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public String getPostHint() {
        return postHint;
    }

    public void setPostHint(String postHint) {
        this.postHint = postHint;
    }

    public Boolean getSelf() {
        return isSelf;
    }

    public void setSelf(Boolean self) {
        isSelf = self;
    }

    public Boolean getHideScore() {
        return hideScore;
    }

    public void setHideScore(Boolean hideScore) {
        this.hideScore = hideScore;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getStickied() {
        return stickied;
    }

    public void setStickied(Boolean stickied) {
        this.stickied = stickied;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthorFlairText() {
        return authorFlairText;
    }

    public void setAuthorFlairText(String authorFlairText) {
        this.authorFlairText = authorFlairText;
    }

    public Boolean getQuarantine() {
        return quarantine;
    }

    public void setQuarantine(Boolean quarantine) {
        this.quarantine = quarantine;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCreatedUtc() {
        return createdUtc;
    }

    public void setCreatedUtc(long createdUtc) {
        this.createdUtc = createdUtc;
    }

    public int getNumComments() {
        return numComments;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public Boolean getVisited() {
        return visited;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }

    public int getUps() {
        return ups;
    }

    public void setUps(int ups) {
        this.ups = ups;
    }

    public Object getReplies() {
        return replies;
    }

    public void setReplies(Object replies) {
        this.replies = replies;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
