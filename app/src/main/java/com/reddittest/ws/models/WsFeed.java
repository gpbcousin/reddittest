
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WsFeed implements Parcelable{

    @SerializedName("kind")
    private String kind;
    @SerializedName("data")
    private WsData data;


    public WsFeed() {
    }

    public WsFeed(String kind, WsData data) {
        this.kind = kind;
        this.data = data;
    }

    protected WsFeed(Parcel in) {
        kind = in.readString();
        data = in.readParcelable(WsData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kind);
        dest.writeParcelable(data, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsFeed> CREATOR = new Creator<WsFeed>() {
        @Override
        public WsFeed createFromParcel(Parcel in) {
            return new WsFeed(in);
        }

        @Override
        public WsFeed[] newArray(int size) {
            return new WsFeed[size];
        }
    };

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public WsData getData() {
        return data;
    }

    public void setData(WsData data) {
        this.data = data;
    }

}
