
package com.reddittest.ws.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WsPreview implements Parcelable{

    @SerializedName("images")
    private List<WsImage> images = new ArrayList<WsImage>();

    protected WsPreview(Parcel in) {
        images = in.createTypedArrayList(WsImage.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(images);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WsPreview> CREATOR = new Creator<WsPreview>() {
        @Override
        public WsPreview createFromParcel(Parcel in) {
            return new WsPreview(in);
        }

        @Override
        public WsPreview[] newArray(int size) {
            return new WsPreview[size];
        }
    };

    public List<WsImage> getImages() {
        return images;
    }

    public void setImages(List<WsImage> images) {
        this.images = images;
    }


}
