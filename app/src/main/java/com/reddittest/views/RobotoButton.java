package com.reddittest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.reddittest.R;


public class RobotoButton extends Button {

    public RobotoButton(Context context) {
        super(context);
    }

    public RobotoButton(Context context, AttributeSet attrs) {

        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.Roboto);
        String fontName = styledAttrs.getString(R.styleable.Roboto_typeface);
        styledAttrs.recycle();

        setTypeface(fontName);
    }

    private void setTypeface(String fontName) {
        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
            setTypeface(typeface);
        }
    }
}
