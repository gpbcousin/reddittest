package com.reddittest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

import com.reddittest.R;


public class RobotoSwitchCompat extends SwitchCompat {

    public RobotoSwitchCompat(Context context) {
        super(context);
    }

    public RobotoSwitchCompat(Context context, AttributeSet attrs) {

        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.Roboto);
        String fontName = styledAttrs.getString(R.styleable.Roboto_typeface);
        styledAttrs.recycle();

        setTypeface(fontName);
    }

    private void setTypeface(String fontName) {
        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
            setTypeface(typeface);
        }
    }
}
