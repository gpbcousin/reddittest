package com.reddittest.utils;

/**
 * Created by gpbcousin on 25/10/16.
 */

public class DateUtils {
    public static final int SECOND_IN_MILLS = 1000;
    public static final int MINUTE_IN_MILLS = 60 * SECOND_IN_MILLS;
    public static final int HOUR_IN_MILLS = 60 * MINUTE_IN_MILLS;
    public static final int DAY_IN_MILLS = 24 * HOUR_IN_MILLS;

    public static String formatElapsedTime(long created) {
        return formatElapsedTime(created, System.currentTimeMillis());
    }

    public static String formatElapsedTime(long created, long curentDate) {

        long elapsedTime = curentDate - created;

        long elapsedDays = elapsedTime / DAY_IN_MILLS;
        elapsedTime %= DAY_IN_MILLS;

        long elapsedHours = elapsedTime / HOUR_IN_MILLS;
        elapsedTime %= HOUR_IN_MILLS;

        long elapsedMinutes = elapsedTime / MINUTE_IN_MILLS;

        if (elapsedDays > 0)
            return elapsedDays > 1 ? String.format("%d dias", elapsedDays) : String.format("%d dia", elapsedDays);

        if (elapsedHours > 0)
            return elapsedHours > 1 ? String.format("%d horas", elapsedHours) : String.format("%d hora", elapsedHours);

        if (elapsedMinutes > 0)
            return elapsedMinutes > 1 ? String.format("%d minutos", elapsedMinutes) : String.format("%d minuto", elapsedMinutes);

        return "Agora";
    }
}
