package com.reddittest.utils;

/**
 * Created by gpbcousin on 24/10/16.
 */

public class StringUtils {

    public static  boolean containYoutubeLink(String str) {
        String[] messageParts = str.replaceAll("\\n", " ").trim().split(" ");
        for (String part : messageParts) {
            if (part.matches("^(?:https?:\\/\\/)?(?:www\\.)?(?:m\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:embed\\/|v\\/|watch\\?v=|watch\\?.+&v=))((\\w|-){11})(?:\\S+)?$")) {
                return true;
            }
        }
        return false;
    }
}
